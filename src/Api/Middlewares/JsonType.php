<?php declare(strict_types = 1);

namespace Api\Middlewares;

use Api\Exceptions\InvalidContentType;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

final class JsonType
{
	public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
	{
		try {
			if ($request->getContentType() !== 'application/json') {
				throw new InvalidContentType();
			}
			$body = (string) $request->getBody();
			if ($body) {
				Json::decode($body);
			}
		} catch (JsonException | InvalidContentType $e) {
			$body = ['message' => 'Malformed request body, please provide valid JSON'];
			$code = 415;
			$newResponse = $response->withJson($body, $code);

			return $newResponse;
		}

		return $next($request, $response);
	}
}
