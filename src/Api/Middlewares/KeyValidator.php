<?php declare(strict_types = 1);

namespace Api\Middlewares;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class KeyValidator
{
	public const STRATEGY_REQUIRED = 'requiredStrategy';
	public const STRATEGY_ALLOWED = 'allowedStrategy';

	private const STRATEGIES_AVAILABLE = [
		self::STRATEGY_ALLOWED,
		self::STRATEGY_REQUIRED,
	];

	private const STRATEGY_RESPONSE_MESSAGE = [
		self::STRATEGY_ALLOWED => 'contains illegal',
		self::STRATEGY_REQUIRED => 'missing required',
	];

	/**
	 * @var array
	 */
	private $keysToCheck;

	/**
	 * @var string
	 */
	private $strategy;


	public function __construct(array $keysToCheck, string $strategy)
	{
		if ( ! in_array($strategy, self::STRATEGIES_AVAILABLE)) {
			throw new \InvalidArgumentException('Unknown key strategy: ' . $strategy);
		}
		$this->keysToCheck = $keysToCheck;
		$this->strategy = $strategy;
	}


	public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
	{
		$requestBody = $request->getParsedBody();

		$danglingKeys = $this->{$this->strategy}($requestBody);

		if (count($danglingKeys) !== 0) {
			$body = ['message' => sprintf(
				'Can\'t process request, body %s keys: %s',
				self::STRATEGY_RESPONSE_MESSAGE[$this->strategy],
				implode(', ', array_keys($danglingKeys)))
			];
			$code = 422;
			$newResponse = $response->withJson($body, $code);

			return $newResponse;
		}

		return $next($request, $response);
	}


	private function requiredStrategy(array $requestBody): array
	{
		return array_diff_key(array_flip($this->keysToCheck), $requestBody);
	}


	private function allowedStrategy(array $requestBody): array
	{
		return array_diff_key($requestBody, array_flip($this->keysToCheck));
	}
}
