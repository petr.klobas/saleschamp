<?php declare(strict_types = 1);

namespace Api\Requests;

use Api\Exceptions\InvalidStateException;
use Nette\Utils\Validators;

final class AddressUpdateRequest
{
	public const STATUS_NOT_AT_HOME = 'not at home';
	public const STATUS_NOT_INTERESTED = 'not interested';
	public const STATUS_INTERESTED = 'interested';

	public const STATUSES_AVAILABLE = [
		self::STATUS_NOT_AT_HOME,
		self::STATUS_NOT_INTERESTED,
		self::STATUS_INTERESTED,
	];

	/**
	 * @var string
	 */
	private $status;

	/**
	 * @var string
	 */
	private $id;

	/**
	 * @var mixed
	 */
	private $email;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var \DateTimeImmutable
	 */
	private $updatedAt;


	public function __construct(string $id, array $requestBody, ?string $currentStatus)
	{
		if ($currentStatus === self::STATUS_INTERESTED || $currentStatus === self::STATUS_NOT_INTERESTED) {
			throw new InvalidStateException('Current state of address doesn\'t allow its further update');
		}

		if ( ! in_array($requestBody['status'], self::STATUSES_AVAILABLE)) {
			throw new \InvalidArgumentException('Invalid address status provided');
		}

		if (array_key_exists('email', $requestBody) && ! Validators::isEmail($requestBody['email'])) {
			throw new \InvalidArgumentException('Provided email is not in valid format');
		}

		$this->id = $id;
		$this->status = $requestBody['status'];
		$this->email = $requestBody['email'];
		$this->name = (string) $requestBody['name'];
		$this->updatedAt = new \DateTimeImmutable();
	}


	public function toArray(): array
	{
		return array_filter([
			'id' => $this->id,
			'status' => $this->status,
			'email' => $this->email,
			'name' => $this->name,
			'updatedAt' => $this->updatedAt->format('Y-m-d\TH:i:s\Z')
		]);
	}
}
