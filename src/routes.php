<?php

use Api\AddressFacade;
use Api\AddressRepository;
use Api\Descriptors\AddressKeys;
use Api\Exceptions\EntityNotFoundException;
use Api\Exceptions\InvalidStateException;
use Api\Middlewares\JsonType;
use Api\Middlewares\KeyValidator;
use Api\Middlewares\Logger;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;


/**
 * - $this within the closure context represents an instance of Slim\App
 * - you can use $this->database which is an instance of MongoDB\Database and $this->logger that represents Monolog\Logger
 */

$app->add(new Logger($container['logger']));
$app->add(new JsonType());

// POST
$app->post('/address', function(Request $request, Response $response) {
	try {
		$requestBody = $request->getParsedBody();
		/** @var AddressFacade $addressFacade */
		$addressFacade = $this->get('addressFacade');
		$body = $addressFacade->createAddress($requestBody);
		$code = 201;
	} catch (InvalidArgumentException $e) {
		$body = ['message' => $e->getMessage()];
		$code = 422;
	}

	$newResponse = $response->withJson($body, $code);

	if ($code === 201) {
		$newResponse = $newResponse->withHeader('Location', '/address/' . $body->getId()); // https://tools.ietf.org/html/rfc7231#section-7.1.2
	}

	return $newResponse;
})->add(new KeyValidator(AddressKeys::POST_REQUIRED_KEYS, KeyValidator::STRATEGY_REQUIRED))
	->add(new KeyValidator(AddressKeys::POST_ALLOWED_KEYS, KeyValidator::STRATEGY_ALLOWED))
;

// PATCH
$app->patch('/address/{id}', function(Request $request, Response $response) {

	$id = $request->getAttribute('id');
	$code = 200;
	$body = [];
	try {
		$requestBody = $request->getParsedBody();
		/** @var AddressFacade $addressFacade */
		$addressFacade = $this->get('addressFacade');
		$body = $addressFacade->updateAddressById($id, $requestBody);
	} catch (EntityNotFoundException | InvalidStateException | InvalidArgumentException $e) {
		if ($e instanceof EntityNotFoundException) {
			$body = ['message' => sprintf('Entity with ID %s not found', $id)];
			$code = 404;
		} elseif ($e instanceof InvalidStateException) {
			$body = ['message' => $e->getMessage()];
			$code = 403;
		} elseif ($e instanceof InvalidArgumentException) {
			$body = ['message' => $e->getMessage()];
			$code = 422;
		}
	}

	$newResponse = $response->withJson($body, $code);

	return $newResponse;

})->add(new KeyValidator(AddressKeys::PATCH_REQUIRED_KEYS, KeyValidator::STRATEGY_REQUIRED))
	->add(new KeyValidator(AddressKeys::PATCH_ALLOWED_KEYS, KeyValidator::STRATEGY_ALLOWED))
;

// DELETE
$app->delete('/address/{id}', function(Request $request, Response $response) {
	/** @var AddressRepository $addressRepository */
	$addressRepository = $this->get('addressRepository');
	$id = $request->getAttribute('id');
	$body = [];
	$code = 204;
	try {
		$result = $addressRepository->deleteById($id);

		if ( ! $result) {
			$body = ['message' => sprintf('Entity with ID %s not found', $id)];
			$code = 404;
		}
	} catch (Throwable $e) {
		$body = ['message' => $e->getMessage()];
		$code = 409;
	}

	$newResponse = $response->withJson($body, $code);

	return $newResponse;
});

// GET
$app->get('/address[/{id}]', function(Request $request, Response $response) {
	/** @var AddressRepository $addressRepository */
	$addressRepository = $this->get('addressRepository');
	$id = $request->getAttribute('id');
	$code = 200;
	if ($id) {
		$entity = $addressRepository->findById($id);
		$body = $entity ?? ['message' => sprintf('Entity with ID %s not found', $id)];
		$code = $entity ? 200 : 404;
		$lastModified = $entity ? $entity->getUpdatedAt() : NULL;
	} else {
		/** @var AddressFacade $addressFacade */
		$addressFacade = $this->get('addressFacade');
		$body = $addressRepository->fetchAll();
		$lastModified = $addressFacade->determineLatestModificationForCollection($body);
	}

	$newResponse = $response->withJson($body, $code);

	if ($lastModified !== NULL) {
		$newResponse = $newResponse->withHeader('Last-modified', $lastModified->format('D, d M Y H:i:s \G\M\T'));
	}

	return $newResponse;
});
