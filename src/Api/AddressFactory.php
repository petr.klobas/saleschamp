<?php declare(strict_types = 1);

namespace Api;

use Api\Requests\AddressUpdateRequest;
use MongoDB\Model\BSONDocument;

final class AddressFactory
{

	public function createStub(array $input): AddressStub
	{
		if ( $input['createdAt'] !== NULL && ! $input['createdAt'] instanceof \DateTimeImmutable) {
			$createdAt = \DateTimeImmutable::createFromFormat('Y-m-d\TH:i:s\Z', $input['createdAt']);
		} else {
			$createdAt = $input['createdAt'];
		}

		if ( $input['updatedAt'] !== NULL && ! $input['updatedAt'] instanceof \DateTimeImmutable) {
			$updatedAt = \DateTimeImmutable::createFromFormat('Y-m-d\TH:i:s\Z', $input['updatedAt']);
		} else {
			$updatedAt = $input['updatedAt'];
		}

		$stub = new AddressStub(
			(string) $input['country'],
			(string) $input['city'],
			(string) $input['street'],
			(string) $input['postalcode'],
			(int) $input['number'],
			$input['numberAddition'],
			$createdAt,
			$updatedAt,
			$input['status'],
			$input['name'],
			$input['email']
		);

		return $stub;
	}


	public function createByIdAndStub(string $id, AddressStub $stub): Address
	{
		return new Address($id, $stub);
	}


	public function createFromBson(BSONDocument $document): Address
	{
		$documentAsArray = $document->getArrayCopy();
		$id = (string) $document['_id'];
		unset($documentAsArray['_id']);
		$stub = $this->createStub($documentAsArray);

		return $this->createByIdAndStub($id, $stub);
	}


	public function createUpdateRequest(string $id, array $requestBody, ?string $currentStatus): AddressUpdateRequest
	{
		return new AddressUpdateRequest($id, $requestBody, $currentStatus);
	}
}
