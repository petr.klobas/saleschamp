<?php declare(strict_types = 1);

namespace Api\Exceptions;

final class EntityNotFoundException extends \Exception
{

}
