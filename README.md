# SalesChamp PHP assignment

This is the basic structure of the project, feel free to extend it in any way you like.

## Directory structure

```
data/       - base set of data that we used to set up your database
logs/       - log files
public/     - accessible via web-server
spec/       - assignment
src/        - source codes for your assignment
vendor/     - third party libraries
```

You should implement your solution in `/src` directory.

## Database

You should fill in database credentials you've received in `src/settings.php` - removed as they shouldn't be in VCS for security reasons.

## Third-party dependencies

```json
"slim/slim": "^3.1",
"slim/php-view": "^2.0",
"monolog/monolog": "^1.17",
"mongodb/mongodb": "^1.0",
"julien-c/iso3166": "^2.0",
"nette/utils": "^3.1"
```
Use composer install

## Deployment

If the solution should be deployed, I would employ different docker image with some industry-standard http server (Apache, Nginx, Lighttpd). Then I would put some docker-aware proxy like Traefik that channel requests to containers and can instrument secure communication via HTTPS.

 
