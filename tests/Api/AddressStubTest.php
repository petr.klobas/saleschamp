<?php declare(strict_types = 1);

namespace ApiTests;

require __DIR__ . '/../bootstrap.php';

use Api\AddressStub;
use Tester\Assert;
use Tester\TestCase;

final class AddressStubTest extends TestCase
{
	public function testInstantiation(): void
	{
		$addressData = $this->provideAddressData();
		$addressData['country'] = 'invalid string';

		Assert::exception(function () use ($addressData) {
			$this->addressStubFactory($addressData);
		}, \InvalidArgumentException::class, 'Invalid country code format, country code must be two-letter string.');

		$addressData['country'] = 'XY';

		Assert::exception(function () use ($addressData) {
			$this->addressStubFactory($addressData);
		}, \InvalidArgumentException::class, 'Invalid country code provided, country doesn\'t exist.');

		$addressData['country'] = 'CZ';
		$addressData['city'] = '';

		Assert::exception(function () use ($addressData) {
			$this->addressStubFactory($addressData);
		}, \InvalidArgumentException::class, 'Invalid city. City must be non empty string.');

		$addressData['city'] = 'Brno';
		$addressData['street'] = '';

		Assert::exception(function () use ($addressData) {
			$this->addressStubFactory($addressData);
		}, \InvalidArgumentException::class, 'Invalid street. Street must be non empty string.');

		$addressData['street'] = 'Some';
		$addressData['postalcode'] = '123ab';

		Assert::exception(function () use ($addressData) {
			$this->addressStubFactory($addressData);
		}, \InvalidArgumentException::class, 'Invalid postal code provided. Postal code must be 5 digits.');

		$addressData['postalcode'] = '12345';
		$addressData['number'] = 0;

		Assert::exception(function () use ($addressData) {
			$this->addressStubFactory($addressData);
		}, \InvalidArgumentException::class, 'Invalid number. Number must be positive integer.');
	}


	private function addressStubFactory(array $addressData)
	{
		return new AddressStub(
			$addressData['country'],
			$addressData['city'],
			$addressData['street'],
			$addressData['postalcode'],
			$addressData['number'],
			$addressData['numberAddition']
		);
	}


	private function provideAddressData(): array
	{
		return [
			'country' => 'CZ',
			'city' => 'Brno',
			'street' => 'Česká',
			'postalcode' => '60200',
			'number' => 203,
			'numberAddition' => '1',
		];
	}
}

(new AddressStubTest())->run();
