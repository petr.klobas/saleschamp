<?php

use Api\AddressFacade;
use Api\AddressRepository;
use Api\AddressFactory;
use MongoDB\Database;

$container = $app->getContainer();

$container['logger'] = function(Interop\Container\ContainerInterface $container) {

    $config = $container->get('settings')['logger'];

    $logger = new Monolog\Logger($config['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($config['path'], $config['level']));

    return $logger;
};

$container['database'] = function(Interop\Container\ContainerInterface $container) {

    $config = $container->get('settings')['mongo'];

    $uri = $server = sprintf('mongodb://%s:%s@%s:%d/%s',
        $config['user'],
        $config['password'],
        $config['host'],
        $config['port'],
        $config['database']
    );
    $client = new MongoDB\Client($uri);

    return $client->selectDatabase($config['database']);
};

$container['addressFactory'] = function(Interop\Container\ContainerInterface $container) {
	return new AddressFactory();
};

$container['addressRepository'] = function(Interop\Container\ContainerInterface $container) {
	/** @var Database $database */
	$database = $container->get('database');
	/** @var AddressFactory $addressFactory */
	$addressFactory = $container->get('addressFactory');

	return new AddressRepository($database, $addressFactory);
};

$container['addressFacade'] = function(Interop\Container\ContainerInterface $container) {
	/** @var AddressRepository $addressRepository */
	$addressRepository = $container->get('addressRepository');
	/** @var AddressFactory $addressFactory */
	$addressFactory = $container->get('addressFactory');

	return new AddressFacade($addressRepository, $addressFactory);
};
