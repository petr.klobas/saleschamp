<?php declare(strict_types = 1);

namespace Api;

use Iso3166\Codes as CountryCode;
use Nette\Utils\Strings;

final class AddressStub
{
	/**
	 * @var string
	 */
	private $country;

	/**
	 * @var string
	 */
	private $city;

	/**
	 * @var string
	 */
	private $street;

	/**
	 * @var string
	 */
	private $postalCode;

	/**
	 * @var int
	 */
	private $number;

	/**
	 * @var string|null
	 */
	private $numberAddition;

	/**
	 * @var \DateTimeInterface|null
	 */
	private $createdAt;

	/**
	 * @var \DateTimeInterface|null
	 */
	private $updatedAt;

	/**
	 * @var string|null
	 */
	private $status;

	/**
	 * @var string|null
	 */
	private $name;

	/**
	 * @var string|null
	 */
	private $email;


	public function __construct(
		string $country,
		string $city,
		string $street,
		string $postalCode,
		int $number,
		?string $numberAddition,
		?\DateTimeInterface $createdAt = NULL,
		?\DateTimeInterface $updatedAt = NULL,
		?string $status = NULL,
		?string $name = NULL,
		?string $email = NULL
	) {
		if ( ! Strings::match($country, '~^[a-zA-Z]{2}$~')) {
			throw new \InvalidArgumentException('Invalid country code format, country code must be two-letter string.');
		}

		if ( ! CountryCode::isValid($country)) {
			throw new \InvalidArgumentException('Invalid country code provided, country doesn\'t exist.');
		}

		if ( ! $city) {
			throw new \InvalidArgumentException('Invalid city. City must be non empty string.');
		}

		if ( ! $street) {
			throw new \InvalidArgumentException('Invalid street. Street must be non empty string.');
		}

		if ( ! Strings::match($postalCode, '~^\d{5}$~')) {
			throw new \InvalidArgumentException('Invalid postal code provided. Postal code must be 5 digits.');
		}

		if ($number < 1) {
			throw new \InvalidArgumentException('Invalid number. Number must be positive integer.');
		}

		if ($createdAt && $updatedAt && $createdAt > $updatedAt) {
			throw new \InvalidArgumentException('Updated timestamp is older than created.');
		}

		$this->country = $country;
		$this->city = $city;
		$this->street = $street;
		$this->postalCode = $postalCode;
		$this->number = $number;
		$this->numberAddition = $numberAddition;
		$this->createdAt = $createdAt;
		$this->updatedAt = $updatedAt ?? $createdAt;
		$this->status = $status;
		$this->name = $name;
		$this->email = $email;
	}


	public function toArray(): array
	{
		return [
			'country' => $this->country,
			'city' => $this->city,
			'street' => $this->street,
			'postalcode' => $this->postalCode,
			'number' => (int) $this->number,
			'numberAddition' => $this->numberAddition,
			'createdAt' => $this->createdAt ? $this->createdAt->format('Y-m-d\TH:i:s\Z') : NULL,
			'updatedAt' => $this->updatedAt ? $this->updatedAt->format('Y-m-d\TH:i:s\Z') : NULL,
			'status' => $this->status,
			'name' => $this->name,
			'email' => $this->email,
		];
	}

	public function getStatus(): ?string
	{
		return $this->status;
	}


	public function getUpdatedAt(): ?\DateTimeInterface
	{
		return $this->updatedAt;
	}
}
