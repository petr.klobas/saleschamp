<?php declare(strict_types = 1);

namespace Api;

final class Address implements \JsonSerializable
{
	/**
	 * @var string
	 */
	private $id;

	/**
	 * @var AddressStub
	 */
	private $addressStub;

	public function __construct(
		string $id,
		AddressStub $addressStub
	) {
		$this->id = $id;
		$this->addressStub = $addressStub;
	}


	public function jsonSerialize(): array
	{
		return ['id' => $this->id] + $this->addressStub->toArray();
	}


	public function getId(): string
	{
		return $this->id;
	}


	public function getStatus(): ?string
	{
		return $this->addressStub->getStatus();
	}


	public function getUpdatedAt(): \DateTimeInterface
	{
		return $this->addressStub->getUpdatedAt();
	}
}
