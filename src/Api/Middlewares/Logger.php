<?php declare(strict_types = 1);

namespace Api\Middlewares;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class Logger
{
	/**
	 * @var \Monolog\Logger
	 */
	private $logger;


	public function __construct(\Monolog\Logger $logger)
	{
		$this->logger = $logger;
	}

	public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
	{
		$this->logger->debug('Incoming request:', [
			'method' => $request->getMethod(),
			'endpoint' => (string) $request->getUri(),
			'headers' => $request->getHeaders(),
			'body' => $request->getParsedBody(),
		]);

		return $next($request, $response);
	}
}
