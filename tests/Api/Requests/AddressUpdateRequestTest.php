<?php declare(strict_types = 1);

namespace ApiTests\Requests;

require __DIR__ . '/../../bootstrap.php';

use Api\Exceptions\InvalidStateException;
use Api\Requests\AddressUpdateRequest;
use Tester\Assert;
use Tester\TestCase;

final class AddressUpdateRequestTest extends TestCase
{
	public function testInstantiation(): void
	{
		$requestBody = $this->provideRequestBody();

		Assert::exception(function() use ($requestBody) {
			new AddressUpdateRequest('123', $requestBody, AddressUpdateRequest::STATUS_NOT_INTERESTED);
		}, InvalidStateException::class);

		$requestBody = $this->provideRequestBody();
		$requestBody['status'] = 'invalid';

		Assert::exception(function() use ($requestBody) {
			new AddressUpdateRequest('123', $requestBody, NULL);
		}, \InvalidArgumentException::class, 'Invalid address status provided');

		$requestBody = $this->provideRequestBody();
		$requestBody['email'] = 'not@email';

		Assert::exception(function() use ($requestBody) {
			new AddressUpdateRequest('123', $requestBody, NULL);
		}, \InvalidArgumentException::class, 'Provided email is not in valid format');
	}


	public function testSerialization(): void
	{
		$requestBody = $this->provideRequestBody();

		$updateRequest = new AddressUpdateRequest('123', $requestBody, AddressUpdateRequest::STATUS_NOT_AT_HOME);
		$toArray = $updateRequest->toArray();
		unset($toArray['updatedAt']);

		Assert::same([
			'id' => '123',
			'status' => 'interested',
			'email' => 'bart@simpson.edu',
		], $toArray);
	}


	private function provideRequestBody(): array
	{
		return [
			'status' => 'interested',
			'email' => 'bart@simpson.edu',
			'name' => NULL,
		];
	}
}

(new AddressUpdateRequestTest())->run();
