<?php declare(strict_types = 1);

namespace Api;

use Api\Exceptions\EntityNotFoundException;

final class AddressFacade
{
	/**
	 * @var AddressRepository
	 */
	private $addressRepository;

	/**
	 * @var AddressFactory
	 */
	private $addressFactory;


	public function __construct(
		AddressRepository $addressRepository,
		AddressFactory $addressFactory
	)
	{
		$this->addressRepository = $addressRepository;
		$this->addressFactory = $addressFactory;
	}


	public function createAddress(array $requestBody): ?Address
	{
		$timestamped = $requestBody + [
			'createdAt' => new \DateTimeImmutable(),
			'updatedAt' => new \DateTimeImmutable(),
		];

		$addressStub = $this->addressFactory->createStub($timestamped);

		return $this->addressRepository->persist($addressStub);
	}


	public function updateAddressById(string $id, array $requestBody): ?Address
	{
		$address = $this->addressRepository->findById($id);

		if ( ! $address) {
			throw new EntityNotFoundException();
		}

		$updateRequest = $this->addressFactory->createUpdateRequest($id, $requestBody, $address->getStatus());

		return $this->addressRepository->updateByRequest($updateRequest);
	}


	/**
	 * @param Address[] $collection
	 */
	public function determineLatestModificationForCollection(array $collection): ?\DateTimeInterface
	{
		if (count($collection) === 0) {
			return NULL;
		}

		$latest = new \DateTimeImmutable('1970-01-01');

		foreach ($collection as $address) {
			if ($address->getUpdatedAt() > $latest) {
				$latest = $address->getUpdatedAt();
			}
		}

		return $latest;
	}
}
