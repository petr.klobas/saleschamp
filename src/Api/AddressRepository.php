<?php declare(strict_types = 1);

namespace Api;

use Api\Requests\AddressUpdateRequest;
use MongoDB\BSON\ObjectId;
use MongoDB\Collection;
use MongoDB\Database;
use MongoDB\Driver\Exception\InvalidArgumentException;
use MongoDB\Operation\FindOneAndUpdate;

final class AddressRepository
{
	/**
	 * @var Database
	 */
	private $database;

	/**
	 * @var AddressFactory
	 */
	private $addressFactory;


	public function __construct(
		Database $database,
		AddressFactory $addressFactory
	)
	{
		$this->database = $database;
		$this->addressFactory = $addressFactory;
	}


	public function persist(AddressStub $addressStub): Address
	{
		$result = $this->collectionFluent()->insertOne($addressStub->toArray());

		return new Address((string) $result->getInsertedId(), $addressStub);
	}


	/**
	 * @return Address[]
	 */
	public function fetchAll(): array
	{
		$collection = $this->collectionFluent()->find([]);

		$entities = [];

		foreach ($collection as $document) {
			$entities[] = $this->addressFactory->createFromBson($document);
		}

		return $entities;
	}


	public function findById(string $id): ?Address
	{
		try {
			$document = $this->collectionFluent()->findOne(['_id' => new ObjectId($id)]);
		} catch (InvalidArgumentException $e) {
			return NULL;
		}

		return $document ? $this->addressFactory->createFromBson($document) : NULL;
	}


	public function updateByRequest(AddressUpdateRequest $addressUpdateRequest): ?Address
	{
		$request = $addressUpdateRequest->toArray();
		$id = array_shift($request);

		$document = $this->collectionFluent()->findOneAndUpdate(
			['_id' => new ObjectId($id)],
			['$set' => $request],
			['returnDocument' => FindOneAndUpdate::RETURN_DOCUMENT_AFTER]
		);

		return $document ? $this->addressFactory->createFromBson($document) : NULL;
	}


	public function deleteById(string $id): bool
	{
		try {
			$deleteResult = $this->collectionFluent()->deleteOne(['_id' => new ObjectId($id)]);
		} catch (InvalidArgumentException $e) {
			return FALSE;
		}

		return $deleteResult->getDeletedCount() === 1;
	}


	private function collectionFluent(): Collection
	{
		return $this->database->addresses;
	}
}
