<?php declare(strict_types = 1);

namespace Api\Descriptors;

final class AddressKeys
{
	private function __construct()
	{

	}

	private const KEY_COUNTRY = 'country';
	private const KEY_CITY = 'city';
	private const KEY_STREET = 'street';
	private const KEY_POSTAL_CODE = 'postalcode';
	private const KEY_NUMBER = 'number';
	private const KEY_NUMBER_ADDITION = 'numberAddition';
	private const KEY_STATUS = 'status';
	private const KEY_NAME = 'name';
	private const KEY_EMAIL = 'email';

	public const POST_REQUIRED_KEYS = [
		self::KEY_COUNTRY,
		self::KEY_CITY,
		self::KEY_STREET,
		self::KEY_POSTAL_CODE,
		self::KEY_NUMBER,
		self::KEY_NUMBER_ADDITION,
	];

	public const POST_ALLOWED_KEYS = self::POST_REQUIRED_KEYS;

	public const PATCH_REQUIRED_KEYS = [
		self::KEY_STATUS,
	];

	public const PATCH_ALLOWED_KEYS = [
		self::KEY_STATUS,
		self::KEY_NAME,
		self::KEY_EMAIL,
	];

}
