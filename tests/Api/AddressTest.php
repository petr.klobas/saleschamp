<?php declare(strict_types = 1);

namespace ApiTests;

require __DIR__ . '/../bootstrap.php';

use Api\Address;
use Api\AddressStub;
use Nette\Utils\Json;
use Tester\Assert;
use Tester\TestCase;

final class AddressTest extends TestCase
{

	public function testSerialization(): void
	{
		$expected = Json::encode($this->provideAddressData());

		Assert::same($expected, Json::encode($this->createAddress()));
	}


	private function createAddressStub(): AddressStub
	{
		return new AddressStub(
			'CZ',
			'Brno',
			'Česká',
			'60200',
			203,
			'1'
		);
	}


	private function createAddress(): Address
	{
		return new Address('123', $this->createAddressStub());
	}


	private function provideAddressData(): array
	{
		return [
			'id' => '123',
			'country' => 'CZ',
			'city' => 'Brno',
			'street' => 'Česká',
			'postalcode' => '60200',
			'number' => 203,
			'numberAddition' => '1',
			'createdAt' => NULL,
			'updatedAt' => NULL,
			'status' => NULL,
			'name' => NULL,
			'email' => NULL,
		];
	}
}

(new AddressTest())->run();
